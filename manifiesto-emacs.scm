(use-package-modules emacs emacs-xyz)

(packages->manifest
 (list 
  ;; emacs-pgtk-xwidgets
  emacs-vundo
  emacs-avy
  emacs-pgtk-xwidgets
  emacs-org
  emacs-ef-themes
  emacs-auctex
  emacs-cdlatex
  emacs-all-the-icons
  emacs-nerd-icons
  emacs-org-bullets
  emacs-vertico
  emacs-diminish
  emacs-multiple-cursors
  emacs-beacon
  emacs-org-roam
  emacs-org-modern
  emacs-org-fragtog
  emacs-doom-modeline
  emacs-hide-mode-line
  emacs-ellama
  emacs-company
  ;; company-box tiene errores
  ;; emacs-company-box
  emacs-company-auctex
  emacs-company-math
  emacs-yasnippet
  emacs-marginalia
  emacs-dashboard
  ;; Usa una salida especÃ­fica del paquete.
  ))     

